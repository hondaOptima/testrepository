function Model(){
	this.actual = -1;
	this.songs = [];
	this._isPlaying = false;
};

Model.prototype = new Observable();

//GO TO NEXT SONG
Model.prototype.goTo = function( index ){
	this.setActive( index );
};
//UPDATE SONGS
Model.prototype.setSongs = function( songs ){
	this.songs = songs;
	this.trigger('songs-changed', this.songs);
};
//GET ACTUALLY SONG
Model.prototype.getActual = function(){
	return this.songs[ this.actual ];
};
//GET ACTUALLY INDEX
Model.prototype.getActualIndex = function(){
	return this.actual;
};
//SET ACTIVE SONG
Model.prototype.setActive = function( index ){
	if( index < 0 || index >= this.songs.length ) index = 0;
	this._isPlaying = true;
	this.actual = index;
	this.trigger('active-song', this.songs[ index ]);
};
//SET SONG STATUS
Model.prototype.setPlaying = function( bool ){
	this.isPlaying = bool;
	if( bool ) this.trigger('playing');
	else this.trigger('paused');
}
//RETURN THE SONG STATUS
Model.prototype.isPlaying = function(){
	return this._isPlaying;
}