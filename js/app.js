'use strict';
var $ = window.jQuery;

$(document).on('ready', init);

function init(){
	$.getJSON('albums.json', function( data ){
		console.log( data );
		var source = $('#album-tmpl').html();
		var template = Handlebars.compile( source );
		var html = template( data );
		$("#list-albums").html( html );
	});
	$("html, body").animate({
		scrollTop : 0
	}, 1000);
}