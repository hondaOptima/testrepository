'use strict';
//MODEL
function Model(){
	this.actual = -1;
	this.songs = [];
	this._isPlaying = false;
};

Model.prototype = new Observable();

//GO TO NEXT SONG
Model.prototype.goTo = function( index ){
	this.setActive( index );
};
//UPDATE SONGS
Model.prototype.setSongs = function( songs ){
	this.songs = songs;
	this.trigger('songs-changed', this.songs);
};
//GET ACTUALLY SONG
Model.prototype.getActual = function(){
	return this.songs[ this.actual ];
};
//GET ACTUALLY INDEX
Model.prototype.getActualIndex = function(){
	return this.actual;
};
//SET ACTIVE SONG
Model.prototype.setActive = function( index ){
	if( index < 0 || index >= this.songs.length ) index = 0
	this._isPlaying = true;
	this.actual = index;
	this.trigger('active-song', this.songs[ index ]);
};
//SET SONG STATUS
Model.prototype.setPlaying = function( bool ){
	this.isPlaying = bool;
	if( !bool ) this.trigger('playing');
	else this.trigger('paused');
}
//RETURN THE SONG STATUS
Model.prototype.isPlaying = function(){
	return this._isPlaying;
}

//CREATE VIEW CLASS
function View() {
	var self = this;

	this.elems = {};
	this.elems.audio = document.createElement('audio');
	document.body.appendChild( this.elems.audio );

	//DOM ELEMENTS
	this.elems.list = $("#Player__songs");
	this.elems.play = $("#play");
	this.elems.next = $("#next");
	this.elems.prev = $("#prev");
	this.elems.avatar = $(".image img");
	this.elems.name = $(".song__name");
	this.elems.author = $(".song__author");

	//DELEGAR EVENTOS ON CLICK
	this.elems.list.on('click', '.song', function(){
		$("#Player__songs li").removeClass('activeSong');
		$(this).addClass('activeSong');
		self.trigger('song-clicked', $(this).attr('data-id') );
	});

	this.elems.play.on('click', function(){
		self.trigger('play-clicked');
		return false;
	});

	this.elems.next.on('click', function(){
		self.trigger('next-clicked');
		return false;
	});

	this.elems.prev.on('click', function(){
		self.trigger('prev-clicked');
		return false;
	});

	//CREAR TEMPLATE DE CANCIONES
	this.template = Handlebars.compile( $("#song-tmpl").html() );
}

View.prototype = new Observable();
//UPDATE LIST
View.prototype.updateList = function( songList ){
	this.elems.list.html( this.template({ songs: songList }));
};
//UPDATE ACTIVE SONG
View.prototype.updateActive = function( song ){
	this.elems.avatar.attr('src', song.img);
	this.elems.name.text( song.Name );
	this.elems.author.text( song.Collaborators );
	$(".playing_now").text( song.Name + ' - ' + song.Collaborators)
	$(".max").text( song.Duration );
	//FUNCIONES DEL REPRODUCTOR
	var minutes = song.Duration.split('.')[0];
	var totalSeconds = parseInt( minutes * 60 ) + parseInt( song.Duration.split('.')[1] );
	this.elems.audio.pause();
	this.elems.audio.src = song.src;
	this.elems.audio.play();
	$(".state").css({
		'width' : 0,
		'animation': totalSeconds + 's state infinite linear'
	});
};
//DAR PLAY A LA CANCION
View.prototype.playSong = function(){
	this.elems.audio.play();
	$( this.elems.audio.play + ' i' ).removeClass('fa-play');

};
//PAUSAR LA CANCION
View.prototype.pauseSong = function(){
	this.elems.audio.pause();
};	


//CREAMOS EL CONTROLADOR
var controller = {
	init : function( playlist ){
		this.playlist = playlist;
		this.artist = '';
		var self = this;

		//SE INSTANCIA EL MODELO Y VISTA NUEVA
		this.model = new Model();
		this.view = new View();

		//ESCUCHAR EVENTOS DEL MODELO
		this.model.on('songs-changed', function( e, songs ){
			self.view.updateList( songs );
		});
		this.model.on('active-song', function( e, song ){
			self.view.updateActive( song );
		});
		this.model.on('playing', function( e, song ){
			//debugger;
			self.view.playSong( song );
		});
		this.model.on('paused', function( e, song ){
			self.view.pauseSong( song );
		});

		//ESCUCHAR LOS EVENTOS DE LA VISTA
		this.view.on('song-clicked', function( e, index ){
			self.model.setActive( index );
		});
		this.view.on('play-clicked', function(){
			self.model.setPlaying( !self.model.isPlaying() );
		});
		this.view.on('next-clicked', function(){
			self.model.goTo( self.model.getActualIndex() + 1 );
		});
		this.view.on('prev-clicked', function(){
			self.model.goTo( self.model.getActualIndex() - 1 );
		});
		if( this.playlist === 'david-guetta-album') {
			this.artist = [
				{
					"Name" : "Dangerous",
					"Duration" : "3.24",
					"Collaborators" : "David Guetta feat Sam Smith, Sam Martin",
					"src": "media/albums/david-guetta/David Guetta - Dangerous.mp3"
				},
				{
					"Name" : "What I Did for Love",
					"Duration" : "3.27",
					"Collaborators" : "David Guetta feat Emeli Sande",
					"src": "media/albums/david-guetta/David Guetta - What I Did For Love.mp3"
				},
				{
					"Name" : "No Money No Love",
					"Duration" : "3.58",
					"Collaborators" : "David Guetta feat Showtek, Elliphant, Ms. Dynamite",
					"src": "media/albums/david-guetta/No Money No Love - David Guetta.mp3"
				},
				{
					"Name" : "Lovers on the Sun",
					"Duration" : "3.22",
					"Collaborators" : "David Guetta feat Sam Martin",
					"src": "media/albums/david-guetta/David Guetta - Lovers On The Sun.mp3"
				},
				{
					"Name" : "Listen",
					"Duration" : "3.47",
					"Collaborators" : "David Guetta feat John Legend",
					"src": "media/albums/david-guetta/David Guetta - Listen.mp3"
				},
				{
					"Name" : "Sun Goes Down",
					"Duration" : "3.32",
					"Collaborators" : "David Guetta feat Showtek, Magic!, Sonny Wilson",
					"src": "media/albums/david-guetta/David Guetta - Sun Goes Down.mp3"
				},
				{
					"Name" : "Bad",
					"Duration" : "2.50",
					"Collaborators" : "David Guetta feat Showtek, Vassy",
					"src": "media/albums/david-guetta/David Guetta & Showtek - Bad.mp3"
				},
				{
					"Name" : "Shot Me Down",
					"Duration" : "3.11",
					"Collaborators" : "David Guetta feat Skylar Grey",
					"src": "media/albums/david-guetta/David Guetta - Shot Me Down.mp3"
				},
				{
					"Name" : "Sexy Bi%$^#h (Dj Chuckie remix)",
					"Duration" : "3.17",
					"Collaborators" : "David Guetta",
					"src": "media/albums/david-guetta/David Guetta - Sexy Bitch.mp3"
				}
			];
		} else if( this.playlist === 'zoe-album'){
			this.artist = [
				{
					"Name" : "Nada",
					"Duration" : "4.40",
					"Collaborators" : "Zoe feat Enrique Bunbury",
					"src" : "media/albums/zoe/nada- zoe ft bunbury.mp3"
				},
				{
					"Name" : "Luna",
					"Duration" : "5.13",
					"Collaborators" : "Zoe feat Lo Blondo",
					"src" : "media/albums/zoe/Luna - zoe.mp3"
				},
				{
					"Name" : "Paula",
					"Duration" : "4.14",
					"Collaborators" : "Zoe ",
					"src" : "media/albums/zoe/zoe - paula.mp3"
				},
				{
					"Name" : "No Me Destruyas",
					"Duration" : "3.45",
					"Collaborators" : "Zoe ",
					"src" : "media/albums/zoe/No me destruyas - Zoe.mp3"
				},
				{
					"Name" : "Soñe",
					"Duration" : "3.52",
					"Collaborators" : "Zoe feat Denise Gutiérrez",
					"src" : "media/albums/zoe/Zoe-Soñe.mp3"
				},
				{
					"Name" : "Arrullo de Estrellas",
					"Duration" : "4.15",
					"Collaborators" : "Zoe ",
					"src" : "media/albums/zoe/Zoé - Arrullo De Estrellas.mp3"
				},
				{
					"Name" : "Labios Rotos",
					"Duration" : "4.22",
					"Collaborators" : "Zoe ",
					"src" : "media/albums/zoe/ZOE- Labios Rotos.mp3"
				}
			];
		}
		//CARGAR LAS CANCIONES CON AJAX
		this.model.setSongs( this.artist );
		this.model.setActive( 0 );
	}
}; //END CONTROLLER
