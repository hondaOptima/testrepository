function Observable(){}

Observable.prototype = {
	/**
		*INICIALIZAR EL EVENTO
	**/

	_initEvents : function(){
		if( this._events ) return
			this._events = {
				all : []
			}
	},

	on : function(evt, callback, context){
		evt = evt || 'all'
		this._initEvents()

		if(!this._events[ evt ])
			this._events[ evt ] = []

		this._events[ evt ].push({
			cb : callback,
			ctx : context
		})

		return this;
	},

	off: function(evt, callback, context){
		var callbacks, i, tuple

		evt = evt || 'all'

		this._initEvents()

		callbacks = this._events[evt]

		if( !callbacks ) return this

		for( i = callbacks.length; i--; ){
			tuple = callbacks[i]
			if( tuple.cb == callback && tuple.ctx == context ){
				callbacks.splice( i, 1 )
				return this
			}
		}

		return this
	},
	
	trigger: function( evt ){
		this._initEvents()
		

		var callbacks = this._events[ evt ],
			i, tuple

		if( callbacks ) for( i = callbacks.length; i--; ){
			tuple = callbacks[i]
			tuple.cb.apply( tuple.ctx, arguments )
		}
		
		// Those subscribed to all events shall be notified
		// allways
		if( evt != 'all' ){
			callbacks = this._events[ 'all' ]

			for( i = callbacks.length; i--; ){
				tuple = callbacks[i]
				tuple.cb.apply( tuple.ctx, arguments )
			}
		}	
		return this
	}

}