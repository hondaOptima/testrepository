class Person {
	//CONSTRUCTOR CLASS
	constructor(firstname, lastname, age) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.age = age;
	}

	getFullName(){
		let msg = 'Hola, Mi nombre es:';
		return msg + ' ' + this.firstname + ' ' + this.lastname;
	}

	getAge(){
		return this.age;
	}
}

//INICIALIZAR UNA NUEVA PERSONA
var juanito = new Person('juanito', 'lopez', 25);
var lorena = new Person('Lorena', 'perez', 30);
//OBTENER SU NOMBRE COMPLETO
juanito.getFullName();
lorena.getFullName();
//OBTENER SU EDAD
juanito.getAge();
lorena.getAge();