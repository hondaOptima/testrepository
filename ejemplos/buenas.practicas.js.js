//LAS FUNCIONES SON OBJETOS
function a(){}
typeof a; // function
a instanceof Object // true

//LAS FUNCIONES TIENEN PROPIEDADES
a.name // 'a'
a.constructor // function Function()... 

//PUEDEN ASIGNARSE A UNA VARIABLE
var b = function(){
	return 4;
}

var c = b;
b(); // 4
c(); // 4

//PUEDEN PASARSE COMO ARGUMENTO DE OTRA FUNCION
function e(){
	return 3;
}
function d( arg ){
	return arg();
}

d(e); // 3 , Me devuelve la ejecucion del argumento que recibio

//LAS FUNCIONES PUEDEN RETORNARSE
function o( n ){
	return function( m ){
		return n * m;
	}
}

o(2)(3); // 6

//PROGRAMACION BASADA EN PROTOTIPOS

///PROTOTIPO PERSONA
function Person( firstname, lastname, age){
	this.firstname = firstname;
	this.lastname = lastname;
	this.age = age;
};

//METODO PARA OBTENER SU NOMBRE COMPLETO
Person.prototype.get_full_name = function(){
	return this.firstname + ' ' + this.lastname;
};

//METODO PARA OBTENER SU EDAD
Person.prototype.get_age = function(){
	return this.age;
}

//INICIALIZAR UNA NUEVA PERSONA
var juanito = new Person('juanito', 'lopez', 25);
var lorena = new Person('Lorena', 'perez', 30);
//OBTENER SU NOMBRE COMPLETO
juanito.get_full_name();
lorena.get_full_name();
//OBTENER SU EDAD
juanito.get_age();
lorena.get_age();